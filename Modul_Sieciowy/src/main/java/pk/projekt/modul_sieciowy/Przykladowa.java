
package pk.projekt.modul_sieciowy;

/**
 * Ten komponent powinien móc nawiązać połączenie z aktywnym komponentem Serwera Statystyk i pobierać/przesyłać z/do niego odpowiednie dane.
 * Dokładny format argumentów i przesyłanych danych może być inny, zmień wtedy odpowiednio interfejsy i implementację. Ważne, żeby w GUI dało się
 * bezproblemowo korzystać z funkcjonalności serwera.
 * W specyfikacji moduł ten wykorzystuje interfejs Gracza aby zautoryzować go automatycznie na serwerze, ale nie jest to konieczne.
 */
public class Przykladowa implements pk.projekt.projektinterfaces.ModulSieciowy {

    public void ConnectToServer(String ServerAddress) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    public void Disconnect() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    public Boolean GetConnectionStatus() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    public void SendRequestToServer(Object request) { //typ tego argumentu powinien zależeć od tego, jak serwer będzie przyjmować zapytania i jest raczej do zmiany
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    public Object GetServerResponse() { //tak samo typ tego obiektu zwracanego
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    
}
