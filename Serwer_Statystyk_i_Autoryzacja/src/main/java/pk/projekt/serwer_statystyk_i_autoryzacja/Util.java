/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.projekt.serwer_statystyk_i_autoryzacja;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Mateusz
 */
public class Util {
    
    private static char[] RANDOM_CHARS_ARRAY_WITHOUT_O = { 'A', 'B', 'C', 'D',
            'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
    private static char[] RANDOM_DIGITS_ARRAY_WITHOUT_01 = { '2', '3', '4',
            '5', '6', '7', '8', '9' };

    private static char[] RANDOM_CHARS_AND_DIGITS_ARRAY_WITHOUT_O01 = { 'A',
            'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
            'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '2', '3',
            '4', '5', '6', '7', '8', '9' };
    
    private static long TWENTY_FOUR_HOURS_IN_MICROSECS = 1000L * 1000L * 60L
            * 60L * 24L;
    private static long HOUR_IN_MICROSECS = 1000L * 1000L * 60L * 60L;
    private static long MINUTE_IN_MICROSECS = 1000L * 1000L * 60L;
    private static final long MICROSECS_TO_MILLISECS_DIVISOR = 1000L;
    private static final long MILLISECS_TO_SECS_DIVISOR = 1000L;
    protected static final long MICROSECS_PER_DAY = 24L * 60L * 60L
            * MILLISECS_TO_SECS_DIVISOR * MICROSECS_TO_MILLISECS_DIVISOR;
    
//    public static String generateSessionId(final String user) {
//      final StringBuilder sid = new StringBuilder(user == null ? "" : user);
//        sid.append("#");
//        sid.append(generateHexRandString(15));
//       return sid.toString();
//    }
    
    
     public static String format(final String msg, final Object... args) {
        return "HANDLING REQUEST ** " + String.format(msg, args);
    }

    public static String formatWithUser(final String userId, final String msg,
            final Object... args) {
        return "HANDLING REQUEST ** " + userId + " ** "
                + String.format(msg, args);
    }

    public static String formatResponse(final String msg, final Object... args) {
        return "RESPONSE ** " + String.format(msg, args);
    }

    public static String formatResponseWithUser(final String userId,
            final String msg, final Object... args) {
        return "RESPONSE ** " + userId + " ** " + String.format(msg, args);
    }

    
    
     public static boolean validateEmail(final String email) {
        final Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
        final Matcher m = p.matcher(email);
        return m.matches();
    }


    

    public static String generateMD5Hash(String string) throws NoSuchAlgorithmException  {
        
        MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(string.getBytes());
		byte[] digest = md.digest();
		StringBuffer sb = new StringBuffer();
		for (byte b : digest) {
			sb.append(String.format("%02x", b & 0xff));
		}
        return sb.toString();
        
    }    
    
    
    public static String getFormattedDateHhmmFromMicrosecsDate(
            final long dateInMicrosecs) {
        final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm",
                Locale.getDefault());
        String dStr = sdf.format(new Date(dateInMicrosecs
                / MICROSECS_TO_MILLISECS_DIVISOR));
        return dStr;
    }

    public static String getFormattedDateHhmmssFromMicrosecsDate(
            final long dateInMicrosecs) {
        final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss",
                Locale.getDefault());
        String dStr = sdf.format(new Date(dateInMicrosecs
                / MICROSECS_TO_MILLISECS_DIVISOR));
        return dStr;
    }

    public static String getFormattedDateHhmmssFromMicrosecsDate2(
            final long dateInMicrosecs) {
        final SimpleDateFormat sdf = new SimpleDateFormat("HHmmss",
                Locale.getDefault());
        String dStr = sdf.format(new Date(dateInMicrosecs
                / MICROSECS_TO_MILLISECS_DIVISOR));
        return dStr;
    }

    public static String getFormattedDateTimeDdmmyyyyHhmmssFromMicrosecsDate(
            final long dateInMicrosecs) {
        final SimpleDateFormat sdf = new SimpleDateFormat(
                "dd-MM-yyyy HH:mm:ss", Locale.getDefault());
        String dStr = sdf.format(new Date(dateInMicrosecs
                / MICROSECS_TO_MILLISECS_DIVISOR));
        return dStr;
    }

    public static String getFormattedDateTimeDdmmyyyyHhmmFromMicrosecsDate(
            final long dateInMicrosecs) {
        final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm",
                Locale.getDefault());
        String dStr = sdf.format(new Date(dateInMicrosecs
                / MICROSECS_TO_MILLISECS_DIVISOR));
        return dStr;
    }

    public static String getFormattedDateYyyymmddFromMicrosecsDate(
            final long dateInMicrosecs) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd",
                Locale.getDefault());
        String dStr = sdf.format(new Date(dateInMicrosecs
                / MICROSECS_TO_MILLISECS_DIVISOR));
        return dStr;
    }

    public static String getFormattedDateYyyymmFromMicrosecsDate(
            final long dateInMicrosecs) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM",
                Locale.getDefault());
        String dStr = sdf.format(new Date(dateInMicrosecs
                / MICROSECS_TO_MILLISECS_DIVISOR));
        return dStr;
    }

    
}
