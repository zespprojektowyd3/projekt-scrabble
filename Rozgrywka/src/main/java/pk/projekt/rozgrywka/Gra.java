/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.projekt.rozgrywka;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JTable;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
//import pk.projekt.litera.Litera;
//import pk.projekt.gracz.Gracz;
//import pk.projekt.rozgrywka.Rozgrywka;
@Component
public class Gra extends javax.swing.JFrame implements ActionListener {
    
    JTable jt = new JTable(15,15);
//    List<pk.projekt.projektinterfaces.Litera> woreczek = new ArrayList<>();
//    pk.projekt.projektinterfaces.Gracz gracz1 = new Gracz();
//    pk.projekt.projektinterfaces.Gracz gracz2 = new Gracz();
//    pk.projekt.projektinterfaces.Gracz[] gracze = new pk.projekt.projektinterfaces.Gracz[2];
//    pk.projekt.projektinterfaces.Rozgrywka r;
    @Reference
    pk.projekt.projektInterfaces.Slownik slownik;
    void bindSlownik(pk.projekt.projektInterfaces.Slownik slownik)
    {
        this.slownik = slownik;
    }
    
    
    public Gra() {
        initComponents();
        int licznik = 0;
        JButton[][] buttons = new JButton[15][15];
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 15; j++) {
                JButton b = new JButton("     ");
                b.setSize(30, 30);
                b.setMargin(new Insets(1, 1, 1, 1));
                b.addActionListener(this);
                b.setName(i+"_"+j);
                buttons[i][j] = b; 
                licznik++;
            }
        }
        
        //definicje wygladu pol
        //potrojne premie slowne
        buttons[0][0].setBackground(Color.red);
        buttons[0][7].setBackground(Color.red);
        buttons[0][14].setBackground(Color.red);
        buttons[7][0].setBackground(Color.red);
        buttons[7][14].setBackground(Color.red);
        buttons[14][0].setBackground(Color.red);
        buttons[14][7].setBackground(Color.red);
        buttons[14][14].setBackground(Color.red);
        
        //podwojne literowe
        buttons[0][3].setBackground(Color.CYAN);
        buttons[0][11].setBackground(Color.CYAN);
        buttons[2][6].setBackground(Color.CYAN);
        buttons[2][8].setBackground(Color.CYAN);
        buttons[3][0].setBackground(Color.CYAN);
        buttons[3][7].setBackground(Color.CYAN);
        buttons[3][14].setBackground(Color.CYAN);
        buttons[6][2].setBackground(Color.CYAN);
        buttons[6][6].setBackground(Color.CYAN);
        buttons[6][8].setBackground(Color.CYAN);
        buttons[6][12].setBackground(Color.CYAN);
        buttons[7][3].setBackground(Color.CYAN);
        buttons[7][11].setBackground(Color.CYAN);
        buttons[8][2].setBackground(Color.CYAN);
        buttons[8][6].setBackground(Color.CYAN);
        buttons[8][8].setBackground(Color.CYAN);
        buttons[8][12].setBackground(Color.CYAN);
        buttons[11][0].setBackground(Color.CYAN);
        buttons[11][7].setBackground(Color.CYAN);
        buttons[11][14].setBackground(Color.CYAN);
        buttons[12][6].setBackground(Color.CYAN);
        buttons[12][8].setBackground(Color.CYAN);
        buttons[14][3].setBackground(Color.CYAN);
        buttons[14][11].setBackground(Color.CYAN);
        
        //potrojne literowe
        buttons[1][5].setBackground(Color.BLUE);
        buttons[1][9].setBackground(Color.BLUE);
        buttons[5][1].setBackground(Color.BLUE);
        buttons[5][5].setBackground(Color.BLUE);
        buttons[5][9].setBackground(Color.BLUE);
        buttons[5][13].setBackground(Color.BLUE);
        buttons[9][1].setBackground(Color.BLUE);
        buttons[9][5].setBackground(Color.BLUE);
        buttons[9][9].setBackground(Color.BLUE);
        buttons[9][13].setBackground(Color.BLUE);
        buttons[13][5].setBackground(Color.BLUE);
        buttons[13][9].setBackground(Color.BLUE);
        
        //podwojne slowne
        buttons[1][1].setBackground(Color.PINK);
        buttons[1][13].setBackground(Color.PINK);
        buttons[2][2].setBackground(Color.PINK);
        buttons[2][12].setBackground(Color.PINK);
        buttons[3][3].setBackground(Color.PINK);
        buttons[3][11].setBackground(Color.PINK);
        buttons[4][4].setBackground(Color.PINK);
        buttons[4][10].setBackground(Color.PINK);
        buttons[10][4].setBackground(Color.PINK);
        buttons[10][10].setBackground(Color.PINK);
        buttons[11][3].setBackground(Color.PINK);
        buttons[11][11].setBackground(Color.PINK);
        buttons[12][2].setBackground(Color.PINK);
        buttons[12][12].setBackground(Color.PINK);
        buttons[13][1].setBackground(Color.PINK);
        buttons[13][13].setBackground(Color.PINK);

        //pole srodkowe premia 2x na start
        buttons[7][7].setBackground(Color.BLACK);
        buttons[7][7].setForeground(Color.white);

        
        jPanel1.setLayout(new GridLayout(15, 15));
        jPanel1.setSize(500, 500);
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 15; j++) {
                jPanel1.add(buttons[i][j]);
            } 
        }
        jt.setVisible(rootPaneCheckingEnabled);
        this.add(jt);
        
        PrzygotujLitery();
        
        //tymczasowe
//        gracze[0] = gracz1;
//        gracze[1] = gracz2;
//        r = new Rozgrywka(gracze, woreczek);
        //---
    }

    private void PrzygotujLitery()
    {
        GenerujLitery('a', 1, 9);
        GenerujLitery('e', 1, 7);
        GenerujLitery('i', 1, 8);
        GenerujLitery('n', 1, 5);
        GenerujLitery('o', 1, 6);
        GenerujLitery('r', 1, 4);
        GenerujLitery('s', 1, 4);
        GenerujLitery('w', 1, 4);
        GenerujLitery('z', 1, 5);
        GenerujLitery('c', 2, 3);
        GenerujLitery('d', 2, 3);
        GenerujLitery('k', 2, 3);
        GenerujLitery('l', 2, 3);
        GenerujLitery('m', 2, 3);
        GenerujLitery('p', 2, 3);
        GenerujLitery('t', 2, 3);
        GenerujLitery('y', 2, 4);
        GenerujLitery('b', 3, 2);
        GenerujLitery('g', 3, 2);
        GenerujLitery('h', 3, 2);
        GenerujLitery('j', 3, 2);
        GenerujLitery('ł', 3, 2);
        GenerujLitery('u', 3, 2);
        GenerujLitery('ą', 5, 1);
        GenerujLitery('ę', 5, 1);
        GenerujLitery('f', 5, 1);
        GenerujLitery('ó', 5, 1);
        GenerujLitery('ś', 5, 1);
        GenerujLitery('ż', 5, 1);
        GenerujLitery('ć', 6, 1);
        GenerujLitery('ń', 7, 1);
        GenerujLitery('ź', 9, 1);
        GenerujLitery('X', 0, 2);
    }
    private void GenerujLitery(char litera, int punkty, int liczbaliter)
    {
        for (int i = 0; i < liczbaliter; i++) {
//            pk.projekt.projektinterfaces.Litera l = new Litera();
//            l.SetLitera(litera, punkty);
//            woreczek.add(l);
        }
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 450, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 450, Short.MAX_VALUE)
        );

        jTextField1.setText("jTextField1");

        jButton1.setText("Sprawdz slowo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel1)))
                .addGap(0, 250, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 134, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jLabel1)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Button1MouseClicked(java.awt.event.MouseEvent evt)
    {
        System.out.println("ffgfgfdgfg");
    }
    
    private void jPanel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseClicked
        
        System.out.println(evt.getSource());
          System.out.println(evt.getPoint().x + "   "+evt.getPoint().y);      
    }//GEN-LAST:event_jPanel1MouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        //button slowo
        Boolean b = slownik.SprawdzSlowo(jTextField1.getText());
        if (b) {
            jLabel1.setText("poprawne");
        } else jLabel1.setText("niepoprawne");
        
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Gra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Gra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Gra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Gra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Gra().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables

    @Override
    public void actionPerformed(ActionEvent ae) {
        System.out.println(((JButton)ae.getSource()).getName());
    }
}
