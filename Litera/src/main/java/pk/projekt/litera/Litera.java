/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.projekt.litera;

/**
 *
 * @author sTreamx
 */
public class Litera implements pk.projekt.projektinterfaces.Litera{
    char litera;
    int punkty;
    public char GetLitera()
    {
        return litera; 
    }
    public int GetPunkty()
    {
        return punkty;
    }
    public void SetLitera(char litera, int punkty)
    { 
        this.litera = litera; 
        this.punkty = punkty;      
    }
}
