/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.projekt.serwer_statystyk_i_autoryzacja;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

/**
 *
 * @author sTreamx
 */
@Component(immediate=true)
@Service
public class Utilities implements pk.projekt.projektInterfaces.Util{
    

    @Override
    public String generateMD5Hash(String s){
        String x = "";
        try {
            x = generatexMD5Hash(s);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        return x;
    }
    
    public String generatexMD5Hash(String string) throws NoSuchAlgorithmException  {
        
        MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(string.getBytes());
		byte[] digest = md.digest();
		StringBuffer sb = new StringBuffer();
		for (byte b : digest) {
			sb.append(String.format("%02x", b & 0xff));
		}
        return sb.toString();
        
    }   

    
    
}
