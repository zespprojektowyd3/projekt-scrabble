/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.projekt.validator_danych;
import pk.utils.ValidatorAndUtils;
/**
 *
 * @author DGosia
 */
public class Validator implements pk.projekt.projektinterfaces.Validator{
    
    public Validator(){}
    
    public Boolean ValidacjaEmail(String adresEmail)
    {
        return ValidatorAndUtils.validateEmail(adresEmail);
    }
    public String ValidacjaNazwyUzytkownika(String nazwaUzytkownika)
    {
        return ValidatorAndUtils.validateUsername(nazwaUzytkownika);
    }
    public String ValidacjaHasla(String haslo)
    {
        return ValidatorAndUtils.validatePassword(haslo);
    }
    
    
    
    
}
