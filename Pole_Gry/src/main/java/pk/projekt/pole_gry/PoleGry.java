/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.projekt.pole_gry;

import pk.projekt.projektinterfaces.*;

/**
 *
 * @author sTreamx
 */
public class PoleGry implements pk.projekt.projektinterfaces.PoleGry{

    Litera polozonaLitera;
    String rodzajPola;
    Boolean premiaWykorzystana = false;
    
    
    public Boolean SetPolozonaLitera(Litera litera) {
        if (polozonaLitera != null) {
            return false;
        }
        else {
            polozonaLitera = litera;
            return true;
        }
    }

    public Litera GetPolozonaLitera() {
        return polozonaLitera;
    }

    public Boolean IsPremiaWykorzystana() {
        return premiaWykorzystana;
    }

    public void SetPremiaWykorzystana() {
        premiaWykorzystana = true;
    }

    public void SetRodzajPola(String rodzaj) {
        rodzajPola = rodzaj;
    }

    public String GetRodzajPola() {
        return rodzajPola;
    }
    
}
