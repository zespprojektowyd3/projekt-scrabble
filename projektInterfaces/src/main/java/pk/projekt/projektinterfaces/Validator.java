/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.projekt.projektInterfaces;

/**
 *
 * @author DGosia
 */
public interface Validator {
    public Boolean ValidacjaEmail(String adresEmail);
    public String ValidacjaNazwyUzytkownika(String nazwaUzytkownika);
    public String ValidacjaHasla(String haslo);
}
