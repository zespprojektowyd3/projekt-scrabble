/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.projekt.projektInterfaces;


public interface ModulSieciowy {
    public void ConnectToServer(String ServerAddress);
    public void Disconnect();
    public Boolean GetConnectionStatus();
    public void SendRequestToServer(Object request); 
    public Object GetServerResponse();
}
