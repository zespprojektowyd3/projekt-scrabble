/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.projekt.projektInterfaces;

/**
 *
 * @author sTreamx
 */
public interface PoleGry {
    public Boolean SetPolozonaLitera(pk.projekt.projektInterfaces.Litera litera);
    public Litera GetPolozonaLitera();
    public Boolean IsPremiaWykorzystana();
    public void SetPremiaWykorzystana();
    public void SetRodzajPola(String rodzaj);
    public String GetRodzajPola();
}
