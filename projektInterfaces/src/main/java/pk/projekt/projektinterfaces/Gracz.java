/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.projekt.projektInterfaces;

import java.util.List;

/**
 *
 * @author sTreamx
 */
public interface Gracz {
    public String getUsername();
    public void SetUsername(String nazwa);
    public void DodajLitery(List<pk.projekt.projektInterfaces.Litera> nowelitery);
    public void ZabierzLitery(List<pk.projekt.projektInterfaces.Litera> literydozabrania);
}
