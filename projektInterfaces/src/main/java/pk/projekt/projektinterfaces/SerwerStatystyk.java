/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.projekt.projektInterfaces;


public interface SerwerStatystyk {
    public Boolean Zarejestruj(String gracz, String credentials);
    public Boolean Autoryzuj(String gracz, String credentials);
    public String SprawdzStatystyki(String gracz);
    public Integer SprawdzPozycjeWRankingu(String gracz);
    public String PobierzRankingGraczy();
    public void UaktualnijPunktacjeGracza(String gracz, Integer punkty, Boolean zwyciestwo);
}
