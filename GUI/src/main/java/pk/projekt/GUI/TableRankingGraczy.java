/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.projekt.GUI;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Mateusz
 */
@Entity
@Table(name = "TableRankingGraczy", catalog = "scrable", schema = "")
@NamedQueries({
    @NamedQuery(name = "TableRankingGraczy.findAll", query = "SELECT t FROM TableRankingGraczy t"),
    @NamedQuery(name = "TableRankingGraczy.findByGracz", query = "SELECT t FROM TableRankingGraczy t WHERE t.gracz = :gracz"),
    @NamedQuery(name = "TableRankingGraczy.findByPunkty", query = "SELECT t FROM TableRankingGraczy t WHERE t.punkty = :punkty"),
    @NamedQuery(name = "TableRankingGraczy.findByIloscGier", query = "SELECT t FROM TableRankingGraczy t WHERE t.iloscGier = :iloscGier"),
    @NamedQuery(name = "TableRankingGraczy.findByIloscZwyciestw", query = "SELECT t FROM TableRankingGraczy t WHERE t.iloscZwyciestw = :iloscZwyciestw")})
public class TableRankingGraczy implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "gracz")
    private String gracz;
    @Column(name = "punkty")
    private Integer punkty;
    @Column(name = "iloscGier")
    private Integer iloscGier;
    @Column(name = "iloscZwyciestw")
    private Integer iloscZwyciestw;

    public TableRankingGraczy() {
    }

    public TableRankingGraczy(String gracz) {
        this.gracz = gracz;
    }

    public String getGracz() {
        return gracz;
    }

    public void setGracz(String gracz) {
        String oldGracz = this.gracz;
        this.gracz = gracz;
        changeSupport.firePropertyChange("gracz", oldGracz, gracz);
    }

    public Integer getPunkty() {
        return punkty;
    }

    public void setPunkty(Integer punkty) {
        Integer oldPunkty = this.punkty;
        this.punkty = punkty;
        changeSupport.firePropertyChange("punkty", oldPunkty, punkty);
    }

    public Integer getIloscGier() {
        return iloscGier;
    }

    public void setIloscGier(Integer iloscGier) {
        Integer oldIloscGier = this.iloscGier;
        this.iloscGier = iloscGier;
        changeSupport.firePropertyChange("iloscGier", oldIloscGier, iloscGier);
    }

    public Integer getIloscZwyciestw() {
        return iloscZwyciestw;
    }

    public void setIloscZwyciestw(Integer iloscZwyciestw) {
        Integer oldIloscZwyciestw = this.iloscZwyciestw;
        this.iloscZwyciestw = iloscZwyciestw;
        changeSupport.firePropertyChange("iloscZwyciestw", oldIloscZwyciestw, iloscZwyciestw);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gracz != null ? gracz.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TableRankingGraczy)) {
            return false;
        }
        TableRankingGraczy other = (TableRankingGraczy) object;
        if ((this.gracz == null && other.gracz != null) || (this.gracz != null && !this.gracz.equals(other.gracz))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pk.project.GUI.TableRankingGraczy[ gracz=" + gracz + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
