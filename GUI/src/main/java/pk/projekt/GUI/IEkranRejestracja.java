package pk.projekt.GUI;

import java.awt.Component;
import java.sql.Connection;

public interface IEkranRejestracja {

	Component getComponent();

	void SetAppFrame(AppFrame appFrame);

	void setCon(Connection con);
 
}
