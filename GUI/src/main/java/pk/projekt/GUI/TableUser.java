/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.projekt.GUI;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Mateusz
 */
@Entity
@Table(name = "TableUser", catalog = "scrable", schema = "")
@NamedQueries({
    @NamedQuery(name = "TableUser.findAll", query = "SELECT t FROM TableUser t"),
    @NamedQuery(name = "TableUser.findByGracz", query = "SELECT t FROM TableUser t WHERE t.gracz = :gracz"),
    @NamedQuery(name = "TableUser.findByHaslo", query = "SELECT t FROM TableUser t WHERE t.haslo = :haslo")})
public class TableUser implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "gracz")
    private String gracz;
    @Column(name = "haslo")
    private String haslo;

    public TableUser() {
    }

    public TableUser(String gracz) {
        this.gracz = gracz;
    }

    public String getGracz() {
        return gracz;
    }

    public void setGracz(String gracz) {
        String oldGracz = this.gracz;
        this.gracz = gracz;
        changeSupport.firePropertyChange("gracz", oldGracz, gracz);
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        String oldHaslo = this.haslo;
        this.haslo = haslo;
        changeSupport.firePropertyChange("haslo", oldHaslo, haslo);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gracz != null ? gracz.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TableUser)) {
            return false;
        }
        TableUser other = (TableUser) object;
        if ((this.gracz == null && other.gracz != null) || (this.gracz != null && !this.gracz.equals(other.gracz))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pk.project.GUI.TableUser[ gracz=" + gracz + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
