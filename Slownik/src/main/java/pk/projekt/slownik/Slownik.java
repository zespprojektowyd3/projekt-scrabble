/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.projekt.slownik;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
/**
 *
 * @author sTreamx
 */
@Component(immediate=true)
@Service
public class Slownik implements pk.projekt.projektInterfaces.Slownik{
    //        ZipFile z = new ZipFile("h");
    String slownikPath = "slowa-win.txt";  
    BufferedReader in;
    List<String> Slownik = new ArrayList<String>();
    
    @Activate
    void activate() throws IOException
    {
        LoadSlownik();
    }
    
    private void LoadSlownik() throws FileNotFoundException, IOException
    {
        Boolean slownikEnd = false;
        in = new BufferedReader(new FileReader(slownikPath));
        while(!slownikEnd)
        {
            String s = in.readLine();
            if (s != null) {
                Slownik.add(s);
            }
            else slownikEnd = true;
        }
    }
    
    public Boolean SprawdzSlowo(String SprawdzaneSlowo) {
        if (Slownik.contains(SprawdzaneSlowo)) {
            return true;
        }
        else return false;
    }
    
}
