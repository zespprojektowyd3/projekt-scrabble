/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.projekt.gracz;
import java.util.ArrayList;
import java.util.List;
import pk.projekt.projektinterfaces.Litera;
/**
 *
 * @author sTreamx
 */
public class Gracz implements pk.projekt.projektinterfaces.Gracz{

    String nazwa;
    List<pk.projekt.projektinterfaces.Litera> litery;
    
    public Gracz()
    {
        this.litery = new ArrayList<Litera>();
    }
    
    public String getUsername() {
        return nazwa;
    }
    
    public void SetUsername(String nazwa)
    {
        this.nazwa = nazwa;
    }
    
    public void DodajLitery(List<pk.projekt.projektinterfaces.Litera> nowelitery)
    {
        for (int i = 0; i < nowelitery.size(); i++) {
            litery.add(nowelitery.get(i));
        }
    }
    
    public void ZabierzLitery(List<pk.projekt.projektinterfaces.Litera> literydozabrania)
    {
        for (int i = 0; i < literydozabrania.size(); i++) {
            litery.remove(literydozabrania.get(i));
        }
    }
    
    
}
